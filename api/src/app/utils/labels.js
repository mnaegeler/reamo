const path = require('path')
const fs = require('fs').promises

async function getLabel (language, labelKey, defaultValue = '') {
  const jsonPath = path.resolve(__dirname, '../..', 'labels', `${language}.json`)
  let labels = await fs.readFile(jsonPath, 'utf8')
  labels = JSON.parse(labels)

  return labels[labelKey] || defaultValue
}

module.exports = { getLabel }
