const util = require('util')
const path = require('path')

const models = require('../models')
const { Model, View, ViewField, ViewAction, ViewRowAction } = models

const exec = util.promisify(require('child_process').exec);
const modelsPath = path.resolve(__dirname, '..', 'models')
const { exportMetadata } = require('../../generators/export')

function camelToSnake (str) {
  return str.replace(/[A-Z]/g, (letter, index) => `${index > 0 ? '_' : ''}${letter.toLowerCase()}`)
}

module.exports = {
  async getModelMetadata (modelName) {
    return await Model.findOne({
      where: {
        modelName,
      },
      include: ['ModelFields', 'ModelValidations']
    })
  },

  async generateModelFile (modelInstance, body, isCreating=false) {
    const modelFields = body['ModelFields'].filter(f => !['createdAt', 'updatedAt', 'deletedAt', 'OwnerId'].includes(f.name))
    
    // Create default views
    if (isCreating === true) {
      const slugName = camelToSnake(modelInstance.modelName)

      const listView = await View.create({
        ModelId: modelInstance.id,
        key: `list-${slugName}`,
        labelKey: modelInstance.modelName,
        type: 'ListView',
        slug: `/${slugName}`,
        showOnMenu: true,
        filters: null,
        include: null,
      })

      await ViewAction.create({
        ViewId: listView.id,
        labelKey: 'create',
        class: 'is-primary',
        type: 'navigation',
        slug: `/${slugName}_form`,
        order: 1,
        position: 'top right',
      })

      await ViewRowAction.bulkCreate([
        {
          ViewId: listView.id,
          labelKey: 'edit',
          class: 'is-link',
          type: 'navigation',
          slug: `/${slugName}_form/:id:`,
          order: 1,
        },
        {
          ViewId: listView.id,
          labelKey: 'remove',
          class: 'has-text-danger',
          type: 'delete',
          order: 2,
          params: { id: ":id:" },
        }
      ])

      const formView = await View.create({
        ModelId: modelInstance.id,
        key: `form-${slugName}`,
        labelKey: modelInstance.modelName,
        type: 'FormView',
        slug: `/${slugName}_form`,
        showOnMenu: false,
        filters: null,
        include: null,
      })

      await ViewAction.bulkCreate([
        {
          ViewId: formView.id,
          labelKey: 'back',
          class: 'is-link is-light',
          type: 'navigation',
          slug: `/${slugName}`,
          order: 1,
          position: 'top left',
        },
        {
          ViewId: formView.id,
          labelKey: 'save',
          class: 'is-info',
          type: 'submit',
          slug: `/${slugName}`,
          order: 1,
          position: 'form end',
        }
      ])

      const fieldsForm = []
      const fieldsList = []
      modelFields.forEach(async (field, index) => {
        if (index < 4) {
          fieldsList.push({
            ModelFieldId: field.id,
            order: index,
            class: 'is-2',
            labelKey: field.name,
          })
        }

        fieldsForm.push({
          ModelFieldId: field.id,
          order: index,
          class: 'is-2',
          labelKey: field.name,
        })
      })

      const createdAt = body['ModelFields'].find(f => f.name === 'createdAt')
      // const updatedAt = body['ModelFields'].find(f => f.name === 'updatedAt')
      const ownerId = body['ModelFields'].find(f => f.name === 'OwnerId')

      const dateFields = [
        {
          ModelFieldId: createdAt.id,
          order: modelFields.length,
          class: 'is-2',
          canUpdate: false,
          labelKey: 'createdAt',
        },
        // {
        //   ModelFieldId: updatedAt.id,
        //   order: modelFields.length + 1,
        //   class: 'is-2',
        //   canUpdate: false,
        //   labelKey: 'updatedAt',
        // },
      ]

      const ownerFields = [
        {
          ModelFieldId: ownerId.id,
          order: modelFields.length,
          class: 'is-2',
          canUpdate: false,
          labelKey: 'OwnerId',
        },
      ]

      const listFields = [...fieldsList, ...dateFields, ...ownerFields].map(f => {
        let nf = { ...f }
        nf.ViewId = listView.id
        return nf
      })

      const formFields = [...fieldsForm, ...dateFields].map(f => {
        let nf = { ...f }
        nf.ViewId = formView.id
        return nf
      })

      try {
        await ViewField.bulkCreate([...listFields, ...formFields])

        await exportMetadata()
        await exec('node ./src/generators/models.js')
        await exec('node ./src/generators/syncDatabase.js')

        const filePath = path.resolve(modelsPath, `${body.modelName}.js`)

        models[modelInstance.modelName] = models.sequelize['import'](filePath)
        if (models[modelInstance.modelName].associate) {
          models[modelInstance.modelName].associate(models)
        }
      } catch (e) {console.error(e)}
    }
  }

}
