const { User, Role, Language } = require('../models')

const func = function () {
  let _user = null

  const user = () => _user

  const loadUser = async (userId) => {
    if (userId) {
      _user = await User.findByPk(userId, { include: [ Role, Language ]})
    }
  }

  return {
    user,
    loadUser,
  }
}

module.exports = func()
