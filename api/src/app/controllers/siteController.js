require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
});

const models = require('../models');

const siteController = {
  async index (req, res) {
    const bannersQuery = await models['Banner'].findAll({ order: ['order'] });
    const aboutQuery = await models['About'].findAll({ limit: 1 });
    res.render("Reamo Pets.html", { banners: bannersQuery, about: aboutQuery[0] });
  }
}

module.exports = siteController
