const fs = require('fs').promises
const util = require('util')
const path = require('path')
const { Op } = require('sequelize')

const models = require('../models')
const { Auth } = require('../facades')

async function getViewMetadata (role = null) {  
  if (!role) {
    role = {
      id: null,
      hasFullAccess: false,
    }
  }
  
  return await models['View'].findAll({
    attributes: ['id', 'key', 'labelKey', 'type', 'slug', 'showOnMenu', 'filters', 'include'],
    include: [
      {
        model: models['ViewPermission'],
        required: !role.hasFullAccess,
        where: {
          RoleId: {
            [Op.eq]: role.id,
          },
        },
      },

      {
        model: models['Model'],
      },

      {
        model: models['MenuSection'],
      },

      {
        model: models['ViewField'],
        attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
        order: [['order', 'ASC']],
        include: [
          {
            model: models['ModelField'],
            attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
          },
        ],
      },

      {
        model: models['ViewAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
        order: [['order', 'ASC']],
      },

      {
        model: models['ViewRowAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
        order: [['order', 'ASC']],
      },
    ],
  })
}

const metadataController = {
  async getMeta (req, res) {
    const user = Auth.user()
    let languageAbbr = 'en'

    if (user && user.Language && user.Language.active) {
      languageAbbr = user.Language.abbr
    } else if (req.get('Language')) {
      languageAbbr = req.get('Language')
    }

    let labels = null
    let error = ''
    try {
      const jsonPath = path.resolve(__dirname, '../..', 'labels', `${languageAbbr}.json`)
      labels = await fs.readFile(jsonPath, 'utf8')
    } catch (e) {
      error = `Translation file for language ${languageAbbr} not found`
    }

    if (!error) {
      try {
        labels = JSON.parse(labels)
      } catch (e) {
        error = `Translation file for language ${languageAbbr} has syntax errors`
      }
    }

    if (error) {
      console.error(error)
      return res.status(500).send({ messages: [error] })
    }

    try {
      const views = await getViewMetadata(user ? user.Role : null)
      const organizations = await models['OrganizationUser'].findAll({ where: { UserId: user.id }, include: models['Organization'] })

      return res.status(200).send({ views, labels, user, organizations })
    } catch (e) {
      console.error(e)
      return res.status(500).send(e)
    }
  }
}

module.exports = metadataController
