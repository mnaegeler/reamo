module.exports = (sequelize, DataTypes) => {
  const OrganizationUser = sequelize.define("OrganizationUser", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },

    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  OrganizationUser.associate = (models) => {
    OrganizationUser.belongsTo(models.Organization);
    OrganizationUser.belongsTo(models.User);
    OrganizationUser.belongsTo(models.User, {
      as: "Owner",
      constraints: false,
    });
  };

  return OrganizationUser;
};
