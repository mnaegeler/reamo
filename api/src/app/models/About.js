module.exports = (sequelize, DataTypes) => {
  const About = sequelize.define("About", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    description: DataTypes.TEXT,
    image1: DataTypes.STRING,
    image2: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  About.associate = (models) => {
    About.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return About;
};
