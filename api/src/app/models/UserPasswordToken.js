module.exports = (sequelize, DataTypes) => {
  const UserPasswordToken = sequelize.define("UserPasswordToken", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },

    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  UserPasswordToken.associate = (models) => {
    UserPasswordToken.belongsTo(models.User);
    UserPasswordToken.belongsTo(models.User, {
      as: "Owner",
      constraints: false,
    });
  };

  return UserPasswordToken;
};
