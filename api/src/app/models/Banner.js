module.exports = (sequelize, DataTypes) => {
  const Banner = sequelize.define("Banner", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    description: DataTypes.STRING,
    image: DataTypes.STRING,
    link: DataTypes.STRING,
    order: DataTypes.INTEGER,
    title: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Banner.associate = (models) => {
    Banner.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return Banner;
};
