const fs = require('fs').promises
const path = require('path')
// const prettier = require("prettier")

// Paths
const metadataPath = path.resolve(__dirname, '..', 'app', 'metadata')

let models = require('../app/models')
const { Model } = models

async function makeDirs (folder) {
  try {
    await fs.mkdir(path.resolve(metadataPath, folder))
  } catch (e) {}
  
  try {
    await fs.mkdir(path.resolve(metadataPath, folder, 'ModelFields'))
  } catch (e) {}
  
  try {
    await fs.mkdir(path.resolve(metadataPath, folder, 'ModelValidations'))
  } catch (e) {}

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, 'Views'))
  } catch (e) {}
  
  try {
    await fs.mkdir(path.resolve(metadataPath, folder, 'ViewFields'))
  } catch (e) {}

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, 'ViewActions'))
  } catch (e) {}

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, 'ViewRowActions'))
  } catch (e) {}
}

async function writeMetadataModel (model) {
  const filePath = path.resolve(metadataPath, model.modelName, `Model.json`)
  const properties = Object.keys(JSON.parse(JSON.stringify(model)))
    .filter(f => !['ModelFields', 'ModelValidations', 'Views'].includes(f))
    .sort((a, b) => {
      if (a < b) {
        return -1
      }

      if (a > b) {
        return 1
      }

      return 0
    })

  const modelData = {}
  properties.forEach(f => {
    modelData[f] = model[f]
  })

  const fileBuffer = JSON.stringify(modelData, null, 2)
  // const pretty = prettier.format(fileBuffer, { parser: 'json' })
  await fs.writeFile(filePath, fileBuffer)
}

async function writeMetadataModelFields (model) {
  await model.ModelFields.forEach(async field => {
    const filePath = path.resolve(metadataPath, model.modelName, `ModelFields`, `${field.id}.json`)

    const properties = Object.keys(field)
      .sort((a, b) => {
        if (a < b) {
          return -1
        }

        if (a > b) {
          return 1
        }

        return 0
      })

    const fileData = {}
    properties.forEach(prop => {
      fileData[prop] = field[prop]
    })

    const fileBuffer = JSON.stringify(fileData, null, 2)
    await fs.writeFile(filePath, fileBuffer)
  })
}

async function writeMetadataModelValidations (model) {
  await model.ModelValidations.forEach(async field => {
    const filePath = path.resolve(metadataPath, model.modelName, `ModelValidations`, `${field.id}.json`)

    const properties = Object.keys(field)
      .sort((a, b) => {
        if (a < b) {
          return -1
        }

        if (a > b) {
          return 1
        }

        return 0
      })

    const fileData = {}
    properties.forEach(prop => {
      fileData[prop] = field[prop]
    })

    const fileBuffer = JSON.stringify(fileData, null, 2)
    await fs.writeFile(filePath, fileBuffer)
  })
}

async function writeMetadataViews (model) {
  await model.Views.forEach(async view => {
    const filePath = path.resolve(metadataPath, model.modelName, `Views`, `${view.id}.json`)

    const properties = Object.keys(view)
      .filter(prop => !['ViewActions', 'ViewRowActions', 'ViewFields'].includes(prop))
      .sort((a, b) => {
        if (a < b) {
          return -1
        }

        if (a > b) {
          return 1
        }

        return 0
      })

    const fileData = {}
    properties.forEach(prop => {
      fileData[prop] = view[prop]
    })

    const fileBuffer = JSON.stringify(fileData, null, 2)
    await fs.writeFile(filePath, fileBuffer)

    await writeMetadataViewFields(model.modelName, view)
    await writeMetadataViewActions(model.modelName, view)
    await writeMetadataViewRowActions(model.modelName, view)
  })
}

async function writeMetadataViewFields (modelName, view) {
  await view.ViewFields.forEach(async field => {
    const filePath = path.resolve(metadataPath, modelName, `ViewFields`, `${field.id}.json`)

    const properties = Object.keys(field)
      .sort((a, b) => {
        if (a < b) {
          return -1
        }

        if (a > b) {
          return 1
        }

        return 0
      })

    const fileData = {}
    properties.forEach(prop => {
      fileData[prop] = field[prop]
    })

    const fileBuffer = JSON.stringify(fileData, null, 2)
    await fs.writeFile(filePath, fileBuffer)
  })
}

async function writeMetadataViewActions (modelName, view) {
  await view.ViewActions.forEach(async action => {
    const filePath = path.resolve(metadataPath, modelName, `ViewActions`, `${action.id}.json`)

    const properties = Object.keys(action)
      .sort((a, b) => {
        if (a < b) {
          return -1
        }

        if (a > b) {
          return 1
        }

        return 0
      })

    const fileData = {}
    properties.forEach(prop => {
      fileData[prop] = action[prop]
    })

    const fileBuffer = JSON.stringify(fileData, null, 2)
    await fs.writeFile(filePath, fileBuffer)
  })
}

async function writeMetadataViewRowActions (modelName, view) {
  await view.ViewRowActions.forEach(async action => {
    const filePath = path.resolve(metadataPath, modelName, `ViewRowActions`, `${action.id}.json`)

    const properties = Object.keys(action)
      .sort((a, b) => {
        if (a < b) {
          return -1
        }

        if (a > b) {
          return 1
        }

        return 0
      })

    const fileData = {}
    properties.forEach(prop => {
      fileData[prop] = action[prop]
    })

    const fileBuffer = JSON.stringify(fileData, null, 2)
    await fs.writeFile(filePath, fileBuffer)
  })
}

async function exportMetadata () {
  try {
    await fs.rmdir(metadataPath, { recursive: true })
  } catch (e) { console.log(e) }

  try {
    await fs.mkdir(metadataPath)
  } catch (e) {}

  const meta = await Model.findAll({
    include: [
      {
        model: models['ModelField'],
        orderBy: [['createdAt', 'ASC']],
      },
      {
        model: models['ModelValidation'],
        orderBy: [['createdAt', 'ASC']],
      },
      {
        model: models['View'],
        orderBy: [['createdAt', 'ASC']],
        include: [
          {
            model: models['ViewField'],
            orderBy: [['createdAt', 'ASC']]
          },
          {
            model: models['ViewAction'],
            orderBy: [['createdAt', 'ASC']],
          },
          {
            model: models['ViewRowAction'],
            orderBy: [['createdAt', 'ASC']]
          },
        ],
      },
    ],
  })


  try {
    await meta.forEach(async (model) => {
      let m = JSON.parse(JSON.stringify(model))
      await makeDirs(m.modelName)

      await writeMetadataModel(m)
      await writeMetadataModelFields(m)
      await writeMetadataModelValidations(m)
      await writeMetadataViews(m)
    })
  } catch (e) {
    console.error(e)
  }
}

process.argv.forEach(val =>  {
  if (val === '-s') {
    exportMetadata()
  }
})

module.exports = {
  exportMetadata
}
