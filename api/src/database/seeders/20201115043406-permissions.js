'use strict';

const models = require('../../app/models')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Model Permissions - CRUD
     */
    const modelPermissions = {
      // SYSTEM_ADMINISTRATOR has full access, it's not needed to be listed
      SYSTEM_ADMINISTRATOR: null,

      DEFAULT_USER: {
        'Language': ['browse'],
        'Model': ['browse'],
        'Role': ['browse'],
        'User': ['browse', 'show', 'store', 'update', 'delete'],
        'View': ['browse'],
      },
    }

    const modelPermissionsToInsert = []
    for (let roleReference in modelPermissions) {
      if (!modelPermissions[roleReference]) {
        continue
      }

      const role = await models['Role'].findOne({ where: { reference: roleReference } })

      for (let modelName in modelPermissions[roleReference]) {
        const model = await models['Model'].findOne({ where: { modelName: modelName } })

        if (model === null) {
          throw new Error(`Model ${modelName} not found`)
        }

        modelPermissions[roleReference][modelName].forEach(action => {
          modelPermissionsToInsert.push({
            RoleId: role.id,
            ModelId: model.id,
            action,
          })
        })
      }
    }
    
    await queryInterface.bulkInsert('ModelPermissions', modelPermissionsToInsert)

    /**
     * View Permissions - Metadata access
     */
    const viewPermissions = {
      // SYSTEM_ADMINISTRATOR has full access, it's not needed to be listed
      SYSTEM_ADMINISTRATOR: null,

      DEFAULT_USER: null,
    }

    const viewPermissionsToInsert = []
    for (let roleReference in viewPermissions) {
      if (!viewPermissions[roleReference]) {
        continue
      }

      const role = await models['Role'].findOne({ where: { reference: roleReference } })

      for (let viewIndex = 0; viewIndex < viewPermissions[roleReference].length; viewIndex++) {
        const key = viewPermissions[roleReference][viewIndex]
        const view = await models['View'].findOne({ where: { key } })

        viewPermissionsToInsert.push({
          RoleId: role.id,
          ViewId: view.id,
        })
      }
    }
    
    if (viewPermissionsToInsert.length) {
      await queryInterface.bulkInsert('ViewPermissions', viewPermissionsToInsert)
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('ModelPermissions', null, {});
    await queryInterface.bulkDelete('ViewPermissions', null, {});
  }
};
